<?php

namespace WBuilder\Core\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static LIVE()
 * @method static static TEST()
 */
final class Mode extends Enum
{
    const LIVE          =   "live";
    const TEST          =   "test";
}
