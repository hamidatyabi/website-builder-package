<?php


namespace WBuilder\Core;


use Illuminate\Support\ServiceProvider;
use WBuilder\Core\Enums\Mode;

class BuilderServiceProvider extends ServiceProvider
{
    public function boot(Builder $builderLoader)
    {
        $this->offerPublishing();
        $this->registerCommands();

        $this->app->singleton(Builder::class, function ($app) use ($builderLoader) {
            return $builderLoader;
        });
    }
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/website-builder.php',
            'website-builder'
        );

        $this->app->register('WBuilder\Core\BuilderServiceProvider');
    }
    protected function registerCommands()
    {

    }
    protected function offerPublishing()
    {
        echo __DIR__.'/../config/website-builder.php'."\n";

        if (! function_exists('config_path')) {
            // function not available and 'publish' not relevant in Lumen
            return;
        }
        echo 'config published => '.config_path('website-builder.php')."\n";
        $this->publishes([
            __DIR__.'/../config/website-builder.php' => config_path('website-builder.php'),
        ], 'config');

    }
}