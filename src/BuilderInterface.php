<?php


namespace WBuilder\Core;


interface BuilderInterface
{
    /**
     * Get gateway display name
     *
     * This can be used by carts to get the display name for each gateway.
     * @return string
     */
    public function getName();

    /**
     * Define gateway parameters, in the following format:
     *
     * array(
     *     'key' => '', // string variable
     *     'mode' => 'test', // string variable
     * );
     * @return array
     */
    public function getDefaultParameters();
}