<?php

namespace WBuilder\Core\Common\Exception;

/**
 * Website Builder Exception marker interface
 */
interface WBuilderException
{
}
