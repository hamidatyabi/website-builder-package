<?php


namespace WBuilder\Core;

use WBuilder\Core\Enums\Mode;

class Builder extends AbstractBuilder
{

    public function getName()
    {
        return "Website Builder Gateway";
    }
    public function getDefaultParameters()
    {
        return array(
            'authorize_key' => config("website-builder.authorize_key") ?: "key",
            'mode' => Mode::fromValue(config("website-builder.mode") ?: "test")
        );
    }

    public function getAuthorizeKey()
    {
        return $this->getParameter('authorize_key');
    }

    public function setAuthorizeKey($value)
    {
        return $this->setParameter('authorize_key', $value);
    }

    public function getMode()
    {
        return $this->getParameter('mode');
    }

    public function setMode($value)
    {
        return $this->setParameter('mode', $value);
    }

}